(function (angular) {
    'use script';

    angular.module('spDialogUrlManager')
        .constant('SP_URL_DIALOG_QUERY_PARAMS', {
            LOGIN: ['loginOrRegister'],
            PRODUCT: ['catalogProduct', 'product'],
            RECIPE: ['recipe'],
            SPECIAL: ['special'],
            LOYALTY: ['registerLoyalty'],
            EXTERNAL_CART: ['externalCart'],
            EMAIL_VERIFICATION: ['emailVerification'],
            INVITED_USER: ['invitedUser'],
            UPDATE_ORDER_V2: ['updateOrderV2']
        })
        .constant('SP_URL_DIALOG_DATA_QUERY_PARAMS', {
            RECIPE: ['showProductsOnly'],
            EXTERNAL_CART: ['externalName'],
            EMAIL_VERIFICATION: ['code'],
            SPECIAL: ['showFrom'],
            UPDATE_ORDER_V2: ['step', 'orderId', 'isCancelOnClose']
        });
})(angular);